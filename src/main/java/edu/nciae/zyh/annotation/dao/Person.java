package edu.nciae.zyh.annotation.dao;

import org.springframework.stereotype.Repository;


public interface Person {
    public void say();
}
